# moonbased: a fake stock exchange system for shitposting
# Copyright 2021, moonbased team and the moonbased contributors
# SPDX-License-Identifier: AGPL-3.0-only


class APIError(Exception):
    status_code = 500

    def get_payload(self):
        return {}
