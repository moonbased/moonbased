# moonbased: a fake stock exchange system for shitposting
# Copyright 2021, moonbased team and the moonbased contributors
# SPDX-License-Identifier: AGPL-3.0-only

from .index import bp as index

__all__ = ("index",)
