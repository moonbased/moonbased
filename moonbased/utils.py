# moonbased: a fake stock exchange system for shitposting
# Copyright 2021, moonbased team and the moonbased contributors
# SPDX-License-Identifier: AGPL-3.0-only

from typing import Any
import tomlkit


def toml_to_native(value) -> Any:
    """Convert a TOML value to a native type from Python."""

    if isinstance(value, tomlkit.items.String):
        return value._original

    if isinstance(value, tomlkit.items.Integer):
        return int(value)

    if isinstance(value, tomlkit.items.Table):
        dict_doc = dict(value)

        for key, value in dict_doc.items():
            dict_doc[key] = toml_to_native(value)

        return dict_doc

    raise TypeError(f"Value is not supported: {type(value)}")
