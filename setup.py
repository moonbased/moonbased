# moonbased: a fake stock exchange system for shitposting
# Copyright 2021, moonbased team and the moonbased contributors
# SPDX-License-Identifier: AGPL-3.0-only

from setuptools import setup

setup(
    name="moonbased",
    version="0.1.0",
    description="fake shitposting stock echange",
    url="",
    author="TODO",
    python_requires=">=3.9",
    install_requires=[
        "Quart==0.14.1",
        "quart-schema==0.4.0",
        "tomlkit==0.7.0",
        # --------------------------------------------------------------------
        # when changing these dependencies, make sure to change them in
        # .gitlab-ci.yml, too.
        "asyncpg==0.21.0",
        # ---------------------------------------------------------------------
        # "violet @ git+https://gitlab.com/elixire/violet.git@ea5b8373c46dc8f5314ef44f2570c00059d58d3b#egg=violet",
        "hail @ git+https://gitlab.com/elixire/hail.git@d481786d256e682f6992ca250e9f8516205d0608#egg=hail",
        # migrations!
        "agnostic==1.0.2",
        "pg8000>=1.12.3,<1.13.0",
    ],
)
